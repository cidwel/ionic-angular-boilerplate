import { Component, Input, OnInit } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-datatable',
  templateUrl: './datatable.component.html',
  styleUrls: [
    './datatable.component.scss',
  ],
  animations: [
    trigger('rowExpansionTrigger', [
      state('void', style({
        transform: 'translateX(-10%)',
        opacity: 0,
      })),
      state('active', style({
        transform: 'translateX(0)',
        opacity: 1,
      })),
      transition('* <=> *', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)')),
    ]),
  ],
})

export class DatatableComponent implements OnInit {

  data: any[] = [];
  columns?: any[];
  dataKey?: string;
  globalSearch?: boolean;
  extraColumns?: any[];
  summaryText?: string;
  expandedRows?: any[];

  @Input() dataTableConfig: {
    expandedRows?: any[];
    summaryText?: string;
    extraColumns?: any[];
    globalSearch?: boolean;
    dataKey?: string;
    columns?: any[];
    data?: any[];
  };

  _L = null;

  constructor(
  ) {
    this.data = this.dataTableConfig && this.dataTableConfig.data;
    this.columns = this.dataTableConfig && this.dataTableConfig.columns;
    this.dataKey = this.dataTableConfig && this.dataTableConfig.dataKey;
    this.globalSearch = this.dataTableConfig && this.dataTableConfig.globalSearch;
    this.extraColumns = this.dataTableConfig && this.dataTableConfig.extraColumns;
    this.summaryText = this.dataTableConfig && this.dataTableConfig.summaryText;
    this.expandedRows = this.dataTableConfig && this.dataTableConfig.expandedRows;
  }

  ngOnInit() {
  }

}
