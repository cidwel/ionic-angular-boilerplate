import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MenuPage } from './menu.page';

const routes: Routes = [
  {
    path: '',
    component: MenuPage,
    children: [
      {
        path: '',
        loadChildren: '../main/main.module#MainPageModule',
        pathMatch: 'full',
      },
      {
        path: 'main',
        loadChildren: '../main/main.module#MainPageModule',
        pathMatch: 'full',
      },
      {
        path: 'menu',
        loadChildren: '../home/home.module#HomePageModule',
        pathMatch: 'full',
      },
      {
        path: 'second',
        loadChildren: '../second/second.module#SecondPageModule',
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
  ],
  providers: [
  ],
  declarations: [MenuPage],
})
export class MenuPageModule {}
