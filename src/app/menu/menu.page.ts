import { Component, OnInit } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {

  pages = [];

  selectedPath = '';

  constructor(
    private router: Router,
  ) {

    this.pages = [
      {
        title: 'Menu page',
        url: '/menu',
        icon: 'home',
      },
      {
        title: 'Second page',
        url: '/second',
        icon: 'people',
      },
    ];

    this.router.events.subscribe((event: RouterEvent) => {
      debugger;
      if (event && event.url) {
        this.selectedPath = event.url;

      }
    });

  }

  ngOnInit() {

  }

}
